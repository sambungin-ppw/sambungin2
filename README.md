# Tugas 2 | Kelompok 4 PPW - B

### Status Pipelines & Code Coverage
---
[![pipeline status](https://gitlab.com/sambungin-ppw/sambungin2/badges/master/pipeline.svg)](https://gitlab.com/sambungin-ppw/sambungin2/commits/master)

[![coverage report](https://gitlab.com/sambungin-ppw/sambungin2/badges/master/coverage.svg)](https://gitlab.com/sambungin-ppw/sambungin2/commits/master)

### Link to Heroku App
---
Link:
**[Heroku App](http://sambung-in2.herokuapp.com/) : http://sambung-in2.herokuapp.com/**

### Nama-nama Anggota Kelompok
---
Tabel:

Nama Anggota          | NPM
---                   |---
Muhammad Arief Wibowo | 1606837865
Nina Nursita Ramadhan | 1606834573
Nurhikmah Ilmi        | 1606918004
Winda Wijaya          | 1606836704
