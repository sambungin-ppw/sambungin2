from django.apps import AppConfig


class PerusahaanUtamaConfig(AppConfig):
    name = 'perusahaan_utama'
