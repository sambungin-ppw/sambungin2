"""sambungin2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import perusahaan_login.urls as perusahaan_login
import perusahaan_lowongan.urls as perusahaan_lowongan
import perusahaan_profile.urls as perusahaan_profile

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^perusahaan_login/', include(perusahaan_login,namespace='perusahaan_login')),
    url(r'^$', RedirectView.as_view(url='perusahaan_login/', permanent = True), name = '$'),
    url(r'^perusahaan-lowongan/', include(perusahaan_lowongan, namespace='perusahaan-lowongan')),
    url(r'^perusahaan_profile/', include(perusahaan_profile, namespace='perusahaan_profile')),
]
