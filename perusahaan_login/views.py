from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
import requests
# Create your views here.

res = {}
def index(request):
    res['author'] = "Nurhikmah Ilmi"
    html = 'login/login.html'
    return render(request, html, res)

def callback(request):
    print("masuk sini")
    if request.method == "GET":
        code = request.GET.get('code', '')

        url = "https://www.linkedin.com/oauth/v2/accessToken"

        params = {
            "grant_type" : "authorization_code",
            "code" : str(code),
            "redirect_uri" : "http://localhost:8000/perusahaan_login/callback",
            "client_id" : "81zelqgz00u6jy",
            "client_secret" : "o7G9nxtmLw6rQ6FL"
        }

        accessToken = requests.get(url, params=params).json()['access_token']

        params = {
            "oauth2_access_token" : accessToken,
            "format" : "json",
            "is-company-admin" : "true"
        }
        url2 = "https://api.linkedin.com/v1/companies"

        companyId = requests.get(url2, params=params).json()['values'][0]['id']

        params = {
            "oauth2_access_token" : accessToken,
            "format" : "json"
        }
        url3 = "https://api.linkedin.com/v1/companies/"+str(companyId)+":(name,company-type,website-url,logo-url,specialties)"
        print("url13nya yang ini")
        print(url3)
        jr = requests.get(url3, params=params).json()
        print(jr)
        print("Halo")
        tipePerusahaan = jr['companyType']['name']
        logoUrl = jr["logoUrl"]
        name = jr["name"]
        specialties = jr["specialties"]["values"]
        print(type(specialties))
        websiteUrl = jr["websiteUrl"]

        response = {}

        response["name"] = name
        response["logoUrl"] = logoUrl
        response["tipePerusahaan"] = tipePerusahaan
        response["specialties"] = specialties
        response["websiteUrl"] = websiteUrl

        request.session['name'] = name
        request.session['logoUrl'] = logoUrl
        request.session['tipePerusahaan'] = tipePerusahaan
        request.session['websiteUrl'] = websiteUrl

        return render(request, "perusahaan_profile/layout/base.html", response)


    return HttpResponseRedirect(reverse('perusahaan_login:index'))
