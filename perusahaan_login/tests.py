from django.test import TestCase


# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class Sambungin2UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/perusahaan_login/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_func(self):
		found = resolve('/perusahaan_login/')
		self.assertEqual(found.func, index)

	def test_using_callback_func(self):
		found = resolve('/perusahaan_login/callback/')
		self.assertEqual(found.func, callback)
