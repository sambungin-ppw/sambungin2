from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-forum/$', add_forum, name='add-forum'),
    url(r'^add-comment/(?P<id>.*)/$', add_comment, name='add-comment'),
]
