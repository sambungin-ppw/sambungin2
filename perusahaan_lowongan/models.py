from django.db import models

# Create your models here.
# class Perusahaan(models.Model):
#     logo_url = models.URLField(default="https://maxcdn.icons8.com/Share/icon/Users//user_male_circle_filled1600.png")
#     name = models.CharField(max_length=30)
#     tipe_perusahaan = models.CharField(max_length=30)
#     website_url = models.CharField(max_length=100)

class Forum(models.Model):
    # perusahaan = models.ForeignKey(Perusahaan)
    website_url = models.CharField(max_length=100)
    lowongan = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
	# perusahaan = models.ForeignKey(Perusahaan)
	forum = models.ForeignKey(Forum)
	comment = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
