from django import forms

class Forum_Form(forms.Form):
    error_messages = {
        'required': 'is empty',
    }
    description_attrs = {
    'type': 'text',
    'cols': 50,
    'rows': 4,
    'class': 'forum-form-textarea',
    'placeholder':'Masukan deskripsi lowongan kerja'
    }
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

class Comment_Form(forms.Form):
    error_messages = {
        'required': 'is empty',
    }
    description_attrs = {
    'type': 'text',
    'cols': 50,
    'rows': 2,
    'class': 'forum-form-textarea',
    'placeholder':'Masukan comment...'
    }
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
