from django.shortcuts import render
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from .forms import Forum_Form, Comment_Form
from .models import Forum, Comment
# from perusahaan_profile.models import Perusahaan


response={}
# Create your views here.
def index(request):
    if 'name' not in request.session:
        return HttpResponseRedirect(reverse('perusahaan_login:index'))
    else:    # pragma: no cover
        perusahaan = request.session['websiteUrl']

        forum = Forum.objects.filter(website_url=perusahaan).order_by('-created_date')
        # forum = Forum.objects.all().order_by('-created_date')
        # forum_haha = Forum.objects.get(lowongan="DFSAF")

        paginator = Paginator(forum, 1)
        page = request.GET.get('page', 1)
        try:
            halaman = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            halaman = paginator.page(1)
        except EmptyPage:
            halaman = paginator.page(paginator.num_pages)

        response['nama_perusahaan'] = request.session['name']
        response['logo_url'] = request.session['logoUrl']
        response['forum_list'] = halaman
        html = 'perusahaan_lowongan/forum.html'
        response['forum_form'] = Forum_Form
        return render(request, html, response)



def add_forum(request): # pragma: no cover
    form = Forum_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['lowongan'] = request.POST['description']
        web_url = request.session['websiteUrl']
        forum = Forum(lowongan=response['lowongan'], website_url=web_url)
        forum.save()
        return HttpResponseRedirect('/perusahaan-lowongan/')
    else:
        return HttpResponseRedirect('/perusahaan-lowongan/')

def add_comment(request, id):   # pragma: no cover
    forum = Forum.objects.get(id=id)
    form = Comment_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['comment'] = request.POST['description']
        comment = Comment(comment=response['comment'], forum=forum)
        comment.save()
        return HttpResponseRedirect('/perusahaan-lowongan/')
    else:
        return HttpResponseRedirect('/perusahaan-lowongan/')
