from django.apps import AppConfig


class PerusahaanLowonganConfig(AppConfig):
    name = 'perusahaan_lowongan'
