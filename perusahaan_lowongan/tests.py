from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *

# Create your tests here.
class testLowongan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/perusahaan-lowongan/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/perusahaan-lowongan/')
        self.assertEqual(found.func, index)

    def test_model_create_forum(self):
        new_forum = Forum.objects.create(lowongan='ole-ole')
        counting_all_available_forum = Forum.objects.all().count()
        self.assertEqual(counting_all_available_forum, 1)

    def test_forum_form_validation_is_blank(self):
        form = Forum_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_comment_form_validation_is_blank(self): # pragma: no cover
        form = Comment_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_comment_form_validation_is_blank(self):
        form = Comment_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_post_forum_render_and_success(self):
        test = 'Tidak'
        session = self.client.session
        session['websiteUrl'] = 'test'
        session.save()

        response = Client().get('/perusahaan-lowongan/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
